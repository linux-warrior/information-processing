(function($) {
    $.fn.activateNavs = function () {
        var $this = $(this);

        $this.each(function () {
            var $nav = $(this);
            $nav.children('li').each(function () {
                var $item = $(this);
                var link = $item.children('a').attr('href');
                if (document.location.pathname.endsWith(link)) {
                    $item.addClass('active');
                    return false;
                }
            });
        });

        return $this;
    };
})(jQuery);
